import { Routes, Route } from 'react-router-dom';
import Nav from "./components/Nav";
import About from "./components/About";
import Footer from "./components/Footer";
import Contact from "./components/Contact";
import Resume from "./components/Resume";
import ProjectsList from './components/ProjectsList';
import Projects from "./components/Projects";
import Art from "./components/Art";
import Home from './components/Home';
import { useEffect, useState } from 'react';



const App = () => {
  const [isNavOpen, setIsNavOpen] = useState(false);

  const toggleNav = () => {
    setIsNavOpen(!isNavOpen);
    console.log(isNavOpen);
  };

  // useEffect(() => {
  //   scrollReveal();
  // });

  return (
    <>
      <div className={`App ${isNavOpen ? 'navOpen' : ''}`}>
        <Nav toggleNav={toggleNav} />
        <div className='body'>
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/about' element={<About />} />
            <Route path='/projects' element={<ProjectsList />} />
            <Route path='/projects/:projectSlug' element={<Projects />} />
            <Route path='/art' element={<Art />} />
            <Route path='/contact' element={<Contact />} />
            <Route path='/resume' element={<Resume />} />
          </Routes>
        </div>
        <Footer />
      </div>
    </>
  )
}

export default App;


    // <div className="App">
    //   <Nav />
    //   <div className='body'>
    //     <About />
    //     <Work />
    //     <Projects />
    //     <Art />
    //     <Contact />
    //   </div>
    //   <Footer />
    // </div>

export default function progressiveImageLoader() {
  if (window.IntersectionObserver) {
    console.log('loadered');

    // Create an IntersectionObserver
    const observer = new IntersectionObserver(onIntersection, {
      root: null, // Use the viewport as the root
      threshold: 0.1, // 10% of the image must be visible
    });

    // Select all elements with class 'progressive replace'
    const pItem = document.querySelectorAll('.progressive.replace');

    // Observe each item
    pItem.forEach((item) => {
      observer.observe(item);
    });

    // Callback when an item enters the viewport
    function onIntersection(entries) {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          console.log('picture in view');
          loadFullImage(entry.target);
          observer.unobserve(entry.target); // Stop observing this item after it's loaded
        }
      });
    }

    // replace with full image
    function loadFullImage(item) {

      if (!item || !item.href) {
        return;
      }

      // load image
      let img = new Image();
      if (item.dataset) {
        img.srcset = item.dataset.srcset || '';
        img.sizes = item.dataset.sizes || '';
      }
      img.src = item.href;
      img.className = 'reveal';
      if (img.complete) {
        addImg();
      } else {
        img.onload = addImg;
      }

      // replace image
      function addImg() {

        // disable click
        item.addEventListener('click', function (e) { e.preventDefault(); }, false);

        // add full image
        item.appendChild(img).addEventListener('animationend', function (e) {

          // remove preview image
          let pImg = item.querySelector && item.querySelector('img.preview');
          if (pImg) {
            e.target.alt = pImg.alt || '';
            item.removeChild(pImg);
            e.target.classList.remove('reveal');
          }

        });

        // Other functions (scroller, loadFullImage, etc.) can remain the same
      }
    }






    

export default function progressiveImageLoader() {

  if (window.addEventListener && window.requestAnimationFrame && document.getElementsByClassName) {
    // window.addEventListener('load', function () {

      console.log('loadered')

      // start
      var pItem = document.getElementsByClassName('progressive replace'), timer;

      window.addEventListener('scroll', scroller, false);
      window.addEventListener('resize', scroller, false);
      inView();


      // throttled scroll/resize
      function scroller(e) {

        timer = timer || setTimeout(function () {
          timer = null;
          requestAnimationFrame(inView);
        }, 300);

      }


      // image in view?
      function inView() {

        var wT = window.scrollY, wB = wT + window.innerHeight, cRect, pT, pB, p = 0;
        // console.log(wT);
        // console.log(wB);

        while (p < pItem.length) {

          cRect = pItem[p].getBoundingClientRect();
          pT = wT + cRect.top;
          pB = pT + cRect.height;

          // console.log(pB);
          // console.log(pT);

          if (wT < pB && wB > pT) {
            console.log('pictures in view')
            loadFullImage(pItem[p]);
            pItem[p].classList.remove('replace');
          }
          else {
            p++;
          }

        }

      }

      setTimeout(function () {
        inView()
      }, 3000)

      // replace with full image
      function loadFullImage(item) {

        if (!item || !item.href) {
          return;
        }

        // load image
        let img = new Image();
        if (item.dataset) {
          img.srcset = item.dataset.srcset || '';
          img.sizes = item.dataset.sizes || '';
        }
        img.src = item.href;
        img.className = 'reveal';
        if (img.complete) {
          addImg();
        } else {
          img.onload = addImg;
        }

        // replace image
        function addImg() {

          // disable click
          item.addEventListener('click', function (e) { e.preventDefault(); }, false);

          // add full image
          item.appendChild(img).addEventListener('animationend', function (e) {

            // remove preview image
            let pImg = item.querySelector && item.querySelector('img.preview');
            if (pImg) {
              e.target.alt = pImg.alt || '';
              item.removeChild(pImg);
              e.target.classList.remove('reveal');
            }

          });

        }

      }

    // }, false);
  }
}

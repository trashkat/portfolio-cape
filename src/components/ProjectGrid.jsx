import { projectDataSoftware, projectDataMech } from "../data/projectData";
import '../css/projects.css'


const ProjectGridSoftware = () => {
  return (
    <>
      <h1>Coding Projects</h1>
      <div className='projectGrid'>
        <div className='projectGridContainer'>
          {projectDataSoftware.map((project, idx) => (
            <div className='projectBox' key={idx} style={{ backgroundImage: `url(${project.imgGrid})` }}>
              <a href={project.href} className='projectBoxTitleDiv' style={{ backgroundColor: project.opaqueColor }}>

                <h1 className='projectBoxTitle'>{project.title}</h1>
              </a>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}

const ProjectGridMech = () => {
  return (
    <>
      <h1>Mech-E Projects</h1>
      <div className='projectGrid'>
        <div className='projectGridContainer'>
          {projectDataMech.map((project, idx) => (
            <div className='projectBox' key={idx} style={{ backgroundImage: `url(${project.imgGrid})` }}>
              <a href={project.href} className='projectBoxTitleDiv' style={{ backgroundColor: project.opaqueColor }}>

                <h1 className='projectBoxTitle'>{project.title}</h1>
              </a>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}

export { ProjectGridSoftware, ProjectGridMech };

import { ProjectItemSoftware, ProjectItemMech } from "./ProjectItem"
import { useEffect, useState } from "react"
import { projectDataSoftware, projectDataMech } from '../data/projectData.jsx'
import '../css/carousel.scss'


const ProjectCarousel = () => {
  const [index, setIndex] = useState(0);
  const amount = projectData.length;

  // const handlePrevious = () => {
  //   const newIndex = index - 1 < 0 ? amount - 1 : index - 1;
  //   setIndex(newIndex);
  // }

  useEffect(() => {
    setIndex(0);
  }, []);

  const handleNext = () => {
    const newIndex = (index + 1 >= amount ? 0 : index + 1);
    // console.log(newIndex);
    setIndex(newIndex);
    // console.log(index);
    // console.log(amount);
  }

  useEffect(() => {
    const interval = setInterval(handleNext, 4000);

    return () => clearInterval(interval);
  });

  return (
    <div className='carousell' style={{ transform: `translateX(0%)` }}>
      {/* <button className='carouselButton' onClick={handlePrevious}>{'<'}</button> */}
      {/* <button onClick={handleNext}>{'>'}</button> */}
      <div className='carouselProject' style={{ transform: `translateX(-${index * 100}%)` }}>
        {projectData.map((project, idx) => (
          <div className='carouselItem' key={idx} style={{ display: idx === index ? 'block' : 'block' }}>
            <img src={project.imgMain} alt={project.title} />
            {/* <p className='carouselTitle'>{project.title}</p> */}
          </div>
        ))}
      </div>
    </div>
  );
}

export default ProjectCarousel

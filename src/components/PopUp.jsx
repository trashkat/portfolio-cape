import { useState } from 'react';
import '../css/popup.css'

const PopUp = () => {

  const [isPopUpVisible, setIsPopUpVisible] = useState(true);

  const togglePopUp = () => {
    setIsPopUpVisible(!isPopUpVisible);
  };

  return (
    isPopUpVisible && (
      <div className='popUpContainer'>
        <div className='popUp'>
          <h3>
            <span>Welcome!</span><br/>
          </h3>
          <p>
            This website is a work in progress: <br/>
            Expect placeholder content <br/> <span>(and a maybe few bugs)</span>
          </p>
          <p>
            Come back soon to see my portfolio evolve, I am always updating it :)
          </p>
        </div>
        <button onClick={togglePopUp} className='popUpClose'>
          <span>X</span>
        </button>
      </div>
    )
  );
}

export default PopUp

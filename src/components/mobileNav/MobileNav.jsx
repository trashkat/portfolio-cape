import { Children, useState, useEffect } from "react"
import { Link, useLocation } from 'react-router-dom'
import { SlSocialInstagram, SlSocialLinkedin, } from 'react-icons/sl'
import { PiGitlabLogoSimple } from 'react-icons/pi'
import Ham from "../animated/ham"
import LinkResume from "./LinkResume";
import LinkCM from "./LinkCM"


const MobileNav = ({ toggleNav }) => {
  const [nav, setNav] = useState(false);
  const [hamburger, setHamburger] = useState(false);
  const location = useLocation();
  const path = location.pathname.substring(1);
  const previousPath = localStorage.getItem('previousPath');

  const [ape, setApe] = useState(path === '' ? 'APE ' : '')
  const [onn, setOnn] = useState(path === '' ? 'ONN' : '')

  // const [ape, setApe] = useState('')
  // const [onn, setOnn] = useState('')

  const [animationState, setAnimationState] = useState(path === '' ? 'false' : 'final');
  const [linkTitle, setLinkTitle] = useState('');

  // Retrieve the linkTitle and animationState from local storage on component mount
  useEffect(() => {
    const storedLinkTitle = localStorage.getItem('linkTitle');
    const storedAnimationState = localStorage.getItem('animationState');

    if (storedLinkTitle) {
      setLinkTitle(storedLinkTitle);
    }

    if (storedAnimationState) {
      setAnimationState(storedAnimationState);
    }
  }, []);

  useEffect(() => {

    console.log('prevpath: ' + previousPath);
    console.log('path: ' + path);
    console.log('animation beg:  ' + animationState)

    if (path === '') {
      setAnimationState('false');
    } else if (path === 'art' && path === previousPath) {
      setLinkTitle('my art');
      setAnimationState('final');
    } else if (path === 'art') {
      setLinkTitle('my art');
      setAnimationState('final');
      localStorage.setItem('previousPath', path);
    } else if (path.includes('projects/') ) {
      setLinkTitle('projects'); 
    } else if (path === previousPath && path !== '') {
      setLinkTitle(path);
      setAnimationState('final');
      return;
    } else if (path !== '') {
      setLinkTitle(path);
      setAnimationState('final');
      localStorage.setItem('previousPath', path);
    } else {
      setLinkTitle(path);
      localStorage.setItem('previousPath', path);
    }

    // Store linkTitle and animationState in local storage
    localStorage.setItem('linkTitle', linkTitle);
    localStorage.setItem('animationState', animationState);

  }, [location]);

  const handleNav = () => {
    setNav(!nav);
    toggleNav();
  };

  const handleLinkCLick = (newTitle) => {
    setNav(false);
    setHamburger(!hamburger);
    setLinkTitle(newTitle);
    toggleNav();
    // setOnn('');
    // setApe('');



    // if (newTitle === 'CM') {
    //   setTimeout(() => {
    //     setLinkTitle('CAPE MONN');
    //   }, 3000);
    // }

    // if (path === previousPath) {
    //   return
    // } else if (animationState === 'false' || animationState === 'initial') {
    //   setAnimationState('final');
    // } else if (animationState === 'final' && linkTitle === '') {
    //   setAnimationState('initial');
    // }

  };

  const handleHam = () => {
    setHamburger(!hamburger);
  }

  return (
    <div className={`mobileNavWrapper ${nav ? 'navOpen' : ''}`}>
      <div id='hamburger' className='mobileNavHeader'>

        <Ham nav={nav} handleNav={handleNav} />
        <div className={`tofill nowrap ${linkTitle !== '' ? 'final center' : ''}`}>
          <h4>{animationState === 'initial' ? '' : linkTitle}</h4>
        </div>
        <LinkCM
          animationState={animationState}
          setAnimationState={setAnimationState}
          setLinkTitle={setLinkTitle}
          linkTitle={linkTitle}
          ape={ape}
          setApe={setApe}
          onn={onn}
          setOnn={setOnn}
        />
        <div className={`hamTwin ${linkTitle !== '' ? 'final' : ''}`}></div>
        {/* <div className='mobileNavMain center'>
          {linkTitle === 'CAPE MONN' ?
            <Link to='/'>
              <h4>CAPE MONN</h4>
            </Link>
            :
            (linkTitle === 'PROJECTS' ?
              <Link to='/projects'>
                <h4 className='dropMain'>PROJECTS</h4>
              </Link>
              :
              <h4 className='noDropMain'>{linkTitle}</h4>
            )
          }
        </div> */}
        {/* <div className={`mobileNavHamTwo ${linkTitle !== 'CAPE MONN' ? 'slideOut' : ''}`}>
          <Link to='/' onClick={() => handleLinkCLick('CAPE MONN')}>
            {linkTitle !== 'CAPE MONN' && <h4 className='dropMain'>CM</h4>}
          </Link>
        </div> */}
      </div>
      {
        // nav ? (
        <div className={`mobileNav ${nav ? 'active' : ''}`}>
          <nav>
            <ul className='mobileNavUl'>
              <li className='mobileNavLi'>
                <Link className='mobileNavLink' onClick={() => handleLinkCLick('RESUME')} to='/resume'>RESUME</Link>
              </li>
              <li className='mobileNavLi'>
                <Link className='mobileNavLink' onClick={() => handleLinkCLick('PROJECTS')} to='/projects'>PROJECTS</Link>
              </li>
              <li className='mobileNavLi'>
                <Link className='mobileNavLink' onClick={() => handleLinkCLick('MY ART')} to='/art'>MY ART</Link>
              </li>
              <li className='mobileNavLi'>
                <Link className='mobileNavLink' onClick={() => handleLinkCLick('CONTACT')} to='/contact'>CONTACT</Link>
              </li>
              <div className='mobileNavSocials'>
                <a onClick={handleLinkCLick} className='socials' href='https://www.instagram.com/catapixie/' target='_blank'>
                  <SlSocialInstagram className='socialsIcon' />
                </a>
                <a onClick={handleLinkCLick} className='socials' href='https://gitlab.com/trashkat' target='_blank'>
                  <PiGitlabLogoSimple className='socialsIcon' />
                </a>
                <a onClick={handleNav} className='socials' href='https://www.linkedin.com/in/cape-monn/' target='_blank'>
                  <SlSocialLinkedin className='socialsIcon' />
                </a>
              </div>
            </ul>
          </nav>
        </div>
        // )
        //   : (
        //     ''
        //   )
      }
    </div>
  )
}

const ResumeContainer = ({ children }) => (
  <div>
    {children}
  </div>
)

export default MobileNav;

import { useState } from 'react'
import { Link } from 'react-router-dom';


const LinkCM = ({ animationState, setAnimationState, linkTitle, setLinkTitle, ape, setApe, onn, setOnn }) => {
  // const [animationState, setAnimationState] = useState('false');


  const handleCapeMonn = () => {

    if (linkTitle === '') {
      setAnimationState('initial');
      return;
    } else {
      if (animationState === 'false' || animationState === 'initial') {
        setAnimationState('final');
        setApe('');
        setOnn('');
        setTimeout(() => {
          setApe('');
          setOnn('');
          console.log('setApe' + setApe);
        }, 1000);
      } else if (animationState === 'final') {
        setAnimationState('initial');
        setLinkTitle('');
        setApe('APE ');
        setOnn('ONN');
      } else {
        setAnimationState('initial');
        setTimeout(() => {
          setApe('APE ');
          setOnn('ONN');
        }, 1000);
      }
    }
  };

  // setTimeout(() => {
  //   setApe('APE');
  //   setOnn('ONN');
  // }, 1000);

  //     setAnimationState('fadePush');
  //     setTimeout(() => {
  //       setAnimationState('fadePush pushed');
  //       setTimeout(() => {
  //         setAnimationState('fadePush pushed rightCorner');
  //       }, 1000); // Delay the rightCorner transition
  //     }, 1000);
  //   } else if (animationState === 'fadePush pushed rightCorner') {
  //     setAnimationState('fadePush pushed');
  //     setTimeout(() => {
  //       setAnimationState('fadePush');
  //       setTimeout(() => {
  //         setAnimationState('initial');
  //       }, 1000); // Delay the initial transition
  //     }, 1000);
  //   }
  // };

  return (
    <Link to='/' className={`capemonn ${animationState}`}>
      {/* <h4 className='nowrap' onClick={handleCapeMonn}> */}
      <h4 className='nowrap' onClick={handleCapeMonn}>
        <span>
          C
          <span>
            {ape}
          </span>
          M
          <span>
            {onn}
          </span>
        </span>
      </h4>
    </Link>
  );
};

export default LinkCM;


// {
//   animationState === 'final' ?
//   <>CM</>
//   :
//   <span>C<span>APE </span>M<span>ONN</span></span>
// }



// const LinkCM = () => {
//   const [capeMonn, setCapeMonn] = useState('CAPE MONN');
//   const [clicked, setClicked] = useState(false);

//   const handleCapeMonn = () => {
//     setClicked(!clicked);
//     // setTimeout(() => {
//     //   setClicked(false);
//     // }, 500);
//   }

//   return (
//     // <div className={`capemonn ${devClick ? '' : 'rightCorner'} `}>

//     <Link to='/' className={`capemonn ${clicked ? "fadePush" : ''}`}>
//         <h4 className={`${clicked ? 'rightCorner' : ''}`} onClick={handleCapeMonn}>
//           C<span className={clicked ? 'pushed' : ''}>APE </span>
//           M<span className={clicked ? 'pushed' : ''}>ONN</span>
//         </h4>
//     </Link>

//   )
// }

// export default LinkCM;

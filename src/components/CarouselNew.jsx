import artData from '../data/artData.jsx'
import '../css/carousel.scss'

const CarouselNew = () => {
  return (
    <div className='slider'>
      <div className='slideTrack'>
      <div className='slide'>
        <img src={artData[0].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[1].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[2].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[3].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[4].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[5].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[0].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[1].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[2].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[3].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[4].img} alt="" />
      </div>
      <div className='slide'>
        <img src={artData[5].img} alt="" />
      </div>
      </div>
    </div>
  )
}

export default CarouselNew

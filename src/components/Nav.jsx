import { Link } from 'react-router-dom';
import MobileNav from "./mobileNav/MobileNav";
import '../css/nav.css'

const Nav = ({ toggleNav }) => {

  return (
    <div>
      <MobileNav toggleNav={toggleNav} />
      <nav className='navbar'>
        <div className='navbarContainer'>
          <ul className='navbarMenu'>
            <li className='navbarItem'>
              <Link to="/resume">RESUME</Link>
            </li>
            <li className='navbarItem'>
              <Link to="/projects">PROJECTS</Link>
            </li>
          </ul>
          <div className='navbarMain'>
            <a href='/'>
              <h4>CAPE MONN</h4>
            </a>
          </div>
          <ul className='navbarMenu'>
            <li className='navbarItem'>
              <Link to='/art'>MY ART</Link>
            </li>
            <li className='navbarItem'>
              <Link to="/contact">CONTACT</Link>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  )
}

export default Nav

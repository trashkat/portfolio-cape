

const SkillsItem = ({ name, category }) => {
  return (
    <div className='resumeItem'>
      <h3 className='resumeItemTitle uppercase roboto'>{category}</h3>
      <ul className='skillList'>
        <div>
        {name.map(nameItem => {
          return <li className='' key={nameItem}>{nameItem}</li>
        })}
        </div>
      </ul>
    </div>
  )
}

export default SkillsItem;

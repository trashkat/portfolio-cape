import '../../css/ham.css'

const Ham = ({ nav, handleNav }) => {

  return (
    <a className='mobileNavHamOne' onClick={handleNav}>
      <div className={`ham ${nav ? 'open' : ''}`} onClick={handleNav}>
        <span></span>
        <span></span>
      </div>
    </a>
  )
}

export default Ham;

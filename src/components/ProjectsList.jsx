import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { projectDataSoftware, projectDataMech } from '../data/projectData.jsx';
import '../css/projects.css';

const ProjectsList = () => {



  const [showItems, setShowItems] = useState(false);

  return (
    <div className='ProjectList'>
      <h2>software engineering</h2>
      <ul className='projectUl'>
        {projectDataSoftware.map((project, idx) => (
          <li className='projectLi' key={idx}>
            <Link to={project.href} className='mobileNavLink'>{project.title.toUpperCase()}</Link>
            {/* <Link to={project.href} className='mobileNavLink'><span className='projectListSpan'>{[0, idx + 1]}</span>{project.title.toUpperCase()}</Link> */}
          </li>
        ))}
      </ul>
      <h2>Mechanical engineering</h2>
      <ul className='projectUl'>
        {projectDataMech.map((project, idx) => (
          <li className='projectLi' key={idx}>
            <Link to={project.href} className='mobileNavLink'>{project.title.toUpperCase()}</Link>
          </li>
        ))}
      </ul>
      <>
      </>
    </div>
  )
}

export default ProjectsList;

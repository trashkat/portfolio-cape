import React from 'react'

const EducationItem = ({ title, school, time, description }) => {
  return (
    <div className='resumeItem'>
      <div className='resumeItemP'>
        <div className='resumeItemCol'>
          <h3 className='resumeItemTitle uppercase roboto'>{school}</h3>
          <span className='resumeItemSubTitle'>{title}</span>
        </div>
        <span className='resumeItemYear'>{time}</span>
      </div>
    </div>
  )
}

export default EducationItem;

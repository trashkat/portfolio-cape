import ProjectItem from "./ProjectItem"
import { useEffect, useState } from "react"
import projectDataSoftware from '../data/projectData.jsx'
import '../css/carouselContinuous.css'


const ProjectCarousel = () => {

  return (
    <ul className="">
      <div>
        {projectData.map((project, idx) => {
          return <li key={idx}>
            <img src={project.imgMain} alt={project.title} />
          </li>
        })}
      </div>
    </ul>
  )


  //   return (
  //     <div className='carousell' style={{ transform: `translateX(0%)`}}>
  //       {/* <button className='carouselButton' onClick={handlePrevious}>{'<'}</button> */}
  //       {/* <button onClick={handleNext}>{'>'}</button> */}
  //       <div className='carouselProject' style={{ transform: `translateX(-${index * 100}%)`}}>
  //         {projectData.map((project, idx) => (
  //           <div className='carouselItem' key={idx} style={{ display: idx === index ? 'block' : 'block' }}>
  //             <img src={project.imgMain} alt={project.title}/>
  //             <p className='carouselTitle'>{project.title}</p>
  //           </div>
  //         ))}
  //       </div>
  //     </div>
  //   );

  //   const [index, setIndex] = useState(0);
  //   const amount = projectData.length;

  //   // const handlePrevious = () => {
  //   //   const newIndex = index - 1 < 0 ? amount - 1 : index - 1;
  //   //   setIndex(newIndex);
  //   // }

  //   useEffect(() => {
  //     setIndex(0);
  //   }, []);

  //   const handleNext = () => {
  //     const newIndex = (index + 1 >= amount ? 0 : index + 1);
  //     // console.log(newIndex);
  //     setIndex(newIndex);
  //     // console.log(index);
  //     // console.log(amount);
  //   }

  //   useEffect(() => {
  //     const interval = setInterval(handleNext, 4000);

  //     return () => clearInterval(interval);
  //   });




}

export default ProjectCarousel

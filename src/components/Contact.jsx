import '../css/contact.css';
import { useForm, ValidationError } from '@formspree/react';

const Contact = () => {

  window.onbeforeunload = () => {
    for (const form of document.getElementsByTagName('form')) {
      form.reset();
    }
  }

  const [state, handleSubmit] = useForm('mwkdwnae');
  if (state.succeeded) {
    return <p className='getInTouch'>Talk to you soon!</p>;
  }

  return (
    <>
      {/* <p className='contact getInTouch'>Talk to you soon!</p> */}
      <img>
      </img>
      <div id='contact' className='contact'>
        <p className='getInTouch'>
          Let's Create Something
        </p>
        {/* <h2 className='contactH2'>CONTACT</h2> */}
        <form
          onSubmit={handleSubmit}
          id='contactForm'
          className='contactForm'
          action='https://formspree.io/f/mwkdwnae'
          method='POST'
        >
          <fieldset className='contactFieldset'>
            <legend className='offscreen'>Contact Me</legend>
            <p className='contactP'>
              <label className='contactLabel' htmlFor='name'>Name</label> {/* offscreen */}
              <input className='contactInput' type='text' id='name' name='name' placeholder='Henry Jones Jr.' required />
              <ValidationError prefix='Name' field='name' errors={state.errors} />
            </p>
            <p className='contactP'>
              <label className='contactLabel' htmlFor="email">Email</label> {/* offscreen */}
              <input className='contactInput' type="email" name="email" id="email" placeholder="hjones@ac.marshall.edu" required />
              <ValidationError prefix='Email' field='email' errors={state.errors} />
            </p>
            <p className='contactP'>
              <label className='contactLabel' htmlFor="message">Message</label>  {/* offscreen */}
              <textarea className="contactTextArea" name="message" id="message" placeholder="( requested project type,  a haiku about a dog,  an adventure proposition,  etc. )"></textarea>
            </p>
          </fieldset>
          <button className="contactButton uppercase" type="submit" disabled={state.submitting}>
            Send
          </button>
          {/* <button className="contactButton" type="reset">Reset</button> */}
        </form>
        {/* <div id='status' className='success'>Success</div> */}
      </div>
    </>
  )
}

export default Contact

import ArtItem from "./ArtItem";
import artData from '../data/artData.jsx'
import '../css/art.css'
import progressiveImageLoader from '../js/progressiveImage';
import scrollReveal from '../js/scrollReveal';
import { useEffect, useState } from 'react';
import { SlSocialInstagram } from 'react-icons/sl'

const Art = () => {

  const [selectedItem, setSelectedItem] = useState(null)

  useEffect(() => {
    console.log('loaded from APP component')
    progressiveImageLoader();
  });

  // useEffect(() => {
  //   scrollReveal();
  // });

  return (
    <div id='art' className='art'>
      {/* <h2 className='artH2'>Artwork</h2> */}
      <div className='artGridContainer'>
        <div className='artContainer'>
          {artData.map((artItem, idx) => (
            <ArtItem
              key={idx}
              img={artItem.img}
              loadingImg={artItem.loadingImg}
              title={artItem.title}
              description={artItem.description}
              year={artItem.year}
              selected={selectedItem === idx}
              onClick={() => setSelectedItem(idx)}
            />
          ))}
        </div>
      </div>
      <p className='comingSoon'>full website coming soon at catapixie.com</p>
      <a className='insta' href='https://www.instagram.com/catapixie/' target='_blank'>
        <SlSocialInstagram className='socialsIcon' />
      </a>
    </div>
  )
}

export default Art;


{/* <div className='artGridContainer'>
  <div className='artContainer'>
    <div className='artItem'>
      <img />
    </div>
  </div>
</div> */}


{/* <ArtCarousel
arts={data}
/> */}

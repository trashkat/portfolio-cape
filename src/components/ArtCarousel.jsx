import ArtItem from "./ArtItem"
import { useState, useEffect } from "react"
import artData from '../data/artData.jsx'
import '../css/carousel.css'

const ArtCarousel = ( ) => {
  const [index, setIndex] = useState(0);
  const amount = artData.length;

  // const handlePrevious = () => {
  //   const newIndex = index - 1 < 0 ? length - 1 : index - 1;
  //   setIndex(newIndex);
  // }

  useEffect(() => {
    setIndex(0);
  }, []);

  const handleNext = () => {
    const newIndex = (index + 1 >= amount ? 0 : index + 1);
    setIndex(newIndex);
  }

  useEffect(() => {
    const interval = setInterval(handleNext, 4000);
    return () => clearInterval(interval);
  });


  return (
    <div className='carousell' style={{ transform: `translateX(0%)`}}>
      <div className='carouselProject' style={{ transform: `translateX(-${index * 100}%)`}}>
        {artData.map((art, idx) => (
          <div className='carouselItemArt' key={idx} style={{ display: idx === index ? 'block' : 'block' }}>
            <img src={art.img} />
          </div>
        ))}
      </div>
    </div>
  );
}

export default ArtCarousel



  // const handleTouchStart = (e) => {
  //   setTouchStartX(e.touches[0].clientX);
  // };

  // const handleTouchMove = (e) => {
  //   if (touchStartX === null) return;

  //   const touchMoveX = e.touches[0].clientX;
  //   const deltaX = touchMoveX - touchStartX;

  //   if (Math.abs(deltaX) > 50) {
  //     if (deltaX > 0) {
  //       handlePrevious();
  //     } else {
  //       handleNext();
  //     }
  //     setTouchStartX(null);
  //   }
  // };

  // const handleTouchEnd = () => {
  //   setTouchStartX(null);
  // };

//   <div
//   className='carouselGroup'
//   onTouchStart={handleTouchStart}
//   onTouchMove={handleTouchMove}
//   onTouchEnd={handleTouchEnd}
// >

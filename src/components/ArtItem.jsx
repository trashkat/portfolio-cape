import { useEffect } from "react";
// import progressiveImageLoader from "../js/progressiveImage";

const ArtItem = ({ img, title, description, year, loadingImg, onClick, selected }) => {

  const handleClick = () => {
    onClick();
  };

  return (
    <div className={`artItem ${selected ? 'expanded' : ''}`} onClick={handleClick}>
      <a href={img} className='progressive replace'>
        <img src={loadingImg} className='artImg preview' />
      </a>
      <div className='artTitleDiv'>
        <p className='artItemTitle'>{title}</p>
        <p className='artItemYear'>{year}</p>
      </div>
    </div>
  )
}

export default ArtItem


//   < figure className = 'artItemFig' >
// <div className='artItemImgContainer'>
//   <img className='artItemImg' src={img} />
// </div>
// <div className='artItemParagraph'>
//   <figcaption className='artItemCaption'>{title}</figcaption>
//   <p className='artItemDescription'>{description}</p>
// </div>
// </figure >

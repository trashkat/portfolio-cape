import profPic from '../assets/profPicBevBW.png'
import '../css/about.css'

const About = () => {
  return (
    <div className="about" id='about'>
      <figure className='aboutFig'>
        <img className='aboutImg' src={profPic} width={600} />
      </figure>
      <article className='aboutArticle'>
        <h2 className='aboutCape curvedText'><em>Cape</em>? <span>That's a </span>strange name...</h2>
        <p className='aboutP centered'>Yup, that's me!</p><br />
        <p className='aboutP'>
          I'm a Florida native turned Bostonian with a degree in Professional Problem Solving (Mechanical Engineering). After a stint in Aerospace, I embraced change, immersing myself in a <a href="https://www.galvanize.com/" target="_blank"> <em>Coding Bootcamp</em></a>. Today, I work on an array of projects from Web Development and ADHD strategies coaching to illustrating.
        </p>
      </article>
    </div>
  )
}

export default About

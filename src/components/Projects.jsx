import { ProjectItemSoftware, ProjectItemMech } from "./ProjectItem"
import { projectDataSoftware, projectDataMech } from '../data/projectData.jsx'
import { useParams, Link } from 'react-router-dom'
import '../css/pagination.css';


const Projects = () => {

  const { projectSlug } = useParams();

  const allProjectData = [...projectDataSoftware, ...projectDataMech];

  const project = allProjectData.find(project => project.href === `/projects/${projectSlug}`);
  const projectIndex = allProjectData.findIndex(project => project.href === `/projects/${projectSlug}`);

  let back = 'BACK';
  let forward = 'NEXT';
  let nextProject = '';
  let prevProject = '';
  let prevProjectHref = '';
  let nextProjectHref = '';

  console.log(prevProject);
  console.log(nextProject);
  console.log(nextProjectHref);

  if (projectIndex + 1 === allProjectData.length) {
    nextProject = 'all projects';
    nextProjectHref = '/projects';
    forward = 'RETURN TO';
    prevProject = allProjectData[projectIndex - 1].title;
    prevProjectHref = allProjectData[projectIndex - 1].href;
  } else if (projectIndex === 0) {
    nextProject = allProjectData[projectIndex + 1].title;
    nextProjectHref = allProjectData[projectIndex + 1].href;
    prevProject = 'all projects';
    prevProjectHref = '/projects';
  } else {
    nextProject = allProjectData[projectIndex + 1].title;
    nextProjectHref = allProjectData[projectIndex + 1].href;
    prevProject = allProjectData[projectIndex - 1].title;
    prevProjectHref = allProjectData[projectIndex - 1].href;
  }

  console.log(prevProject);
  console.log(nextProject);
  console.log(nextProjectHref);

  if (!project) {
    return <div>Project not found</div>;
  }

  const scrollToTop = () => {
    window.scrollTo(0, 0);
  }

  const toTopOfHome = () => {
    window.scrollTo({
      top: 0,
      behavior: '',
    })
  }

  return (
    <>
      <div className='project' id='projects'>
        <ProjectItemSoftware className=''
          imgMain={project.imgMain}
          imgAbout={project.imgAbout}
          imgHome={project.imgHome}
          imgDarkTheme={project.imgDarkTheme}
          imgMobile={project.imgMobile}
          title={project.title}
          year={project.year}
          techStack={project.techStack}
          description={project.description}
          href={project.href}
          git={project.git}
          textColor={project.textColor}
        />
        <div>{''}</div>
        <div className='pagination'>
          <Link to={`${prevProjectHref}`} className='backLink' onClick={scrollToTop}>
            {back}<span className="paginationSubHeader">{prevProject}</span>
          </Link>
          <Link to='/' className='cm' onClick={toTopOfHome}>
            <h4>CM</h4>
          </Link>
          <Link to={`${nextProjectHref}`} className='forwardLink' onClick={scrollToTop}>
            {forward}<span className="paginationSubHeader">{nextProject}</span>
          </Link>
        </div>
      </div>
    </>
  )
}

export default Projects

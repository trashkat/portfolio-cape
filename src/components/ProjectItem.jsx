

const ProjectItemSoftware = ({ imgMain, imgHome, imgAbout, imgDarkTheme, imgMobile, year, techStack, title, description, href, git, gitPlaceholder, textColor }) => {
  return (
    <>
      <h1 style={{ color: textColor }} className='projectItemTitle'>{title}</h1>
      <div className='projectContainerMain'>
        <div className='divImgMain'>
          <img src={imgMain} className='projectImg' />
          <h1 style={{ color: textColor }} className='projectItemTitleMobile'>{title}</h1>
        </div>
        <div className='divRestMain'>
          <div className='techYear'>
            <p style={{ color: textColor }} >TECH STACK</p>
            <p className='techStack'>{techStack}</p>
          </div>
          <div className='techYear'>
            <p style={{ color: textColor }}>YEAR</p>
            <p>{year}</p>
          </div>
          <p className='projectItemDescription'>{description}</p>
          {git &&
            <a className='projectItemHref' href={git} target='_blank'>
            <p><span>↗ </span>see it on the git</p>
          </a>
          }
        </div>
      </div>
      <div className='projectImgGrid'>
        <div className="homePage">
          <h2 style={{ color: textColor }} className='imgHeader'>HOME PAGE</h2>
          <div className='divImg'>
            <img src={imgHome} className='projectImg' />
          </div>
        </div>
        <div className="aboutPage">
          <h2 style={{ color: textColor }} className='imgHeader'>ABOUT PAGE</h2>
          <div className='divImg'>
            <img src={imgAbout} className='projectImg' />
          </div>
        </div>
        <div className="darkTheme">
          <h2 style={{ color: textColor }} className='imgHeader'>DARK THEME</h2>
          <div className='divImg'>
            <img src={imgDarkTheme} className='projectImg' />
          </div>
        </div>
        <div className="mobile">
          <h2 style={{ color: textColor }} className='imgHeader'>MOBILE</h2>
          <div className='divImg'>
            <img src={imgMobile} className='projectImg' />
          </div>
        </div>
      </div>
    </>
  )
}

const ProjectItemMech = ({});

export { ProjectItemSoftware, ProjectItemMech}

import About from './About.jsx'
import ProjectCarousel from './ProjectCarousel.jsx';
import ArtCarousel from './ArtCarouselContinuous.jsx';
import CarouselNew from './CarouselNew.jsx'
import PopUp from './PopUp.jsx';
import { ProjectGridSoftware, ProjectGridMech } from './ProjectGrid.jsx';

const Home = () => {
  return (
    <>
      <PopUp />
      <About />
      <div className='aboutt'>
        <div className='aboutProjects'>
          <h2 className='aboutH2'>Work</h2>
          <ProjectGridSoftware />
          <ProjectGridMech />
          {/* <ProjectCarousel /> */}
        </div>
        <div className='aboutArt'>
          <h2 className='aboutH2 aboutH2Illustrations'>ILLUSTRATIONS</h2>
          <CarouselNew />
        </div>
      </div>

    </>

  )
}

export default Home;

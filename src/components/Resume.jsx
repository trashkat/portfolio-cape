import SkillsItem from "./SkillsItem"
import EducationItem from "./EducationItem";
import WorkItem from "./WorkItem"
import { experienceData, skillsData, educationData } from "../data/resumeData";
import '../css/resume.css'



const Resume = () => {
  return (
    <div className='resume'>
      <div id='resume' className='work'>
        <h2 className='workH2'>Experience</h2>
        {experienceData.map((workItem, idx) => (
          <WorkItem
            key={idx}
            year={workItem.year}
            title={workItem.title}
            company={workItem.company}
            duration={workItem.duration}
            details={workItem.details}
          />
        ))}
      </div>
      <div className='resumeDiv2'>
        <div className='education'>
          <h2 className='workH2'>Education</h2>
          {educationData.map((educationItem, idx) => (
            <EducationItem
              key={idx}
              title={educationItem.title}
              school={educationItem.school}
              time={educationItem.time}
              description={educationItem.description}
            />
          ))}
        </div>
        <div className='skills'>
          <h2 className='workH2'>Skills</h2>
          {skillsData.map((skillItem, idx) => (
            <SkillsItem
              key={idx}
              name={skillItem.name}
              category={skillItem.category}
            />
          ))}
        </div>
      </div>
    </div>
  )
};

export default Resume;

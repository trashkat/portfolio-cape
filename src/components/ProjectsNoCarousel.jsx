
import ProjectItem from "./ProjectItem"
import pebble_linenink from '../assets/pebble_linenink.jpg'

const data = [
  {
    img: pebble_linenink,
    title: 'Project 1',
    description: 'beep',
    href: ''
  },
  {
    img: pebble_linenink,
    title: 'Project 2',
    description: 'boop',
    href: ''
  },
  {
    img: pebble_linenink,
    title: 'Project 3',
    description: 'boop',
    href: ''
  },
  {
    img: pebble_linenink,
    title: 'Project 4',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.',
    href: ''
  },
]

const Projects = () => {
  return (
    <div className='project' id='projects'>
      <h2 className='projectH2'>Projects</h2>
      <p className='projectP'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda, tenetur vero. Dolore eos asperiores eveniet aspernatur.</p>
      <div className='projectsContainer'>
        {data.map((project, idx) => (
          <ProjectItem key={idx} img={project.img} title={project.title} description={project.description} href={project.href} />
        ))}
      </div>
    </div>
  )
}

export default Projects

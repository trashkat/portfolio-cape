

const WorkItem = ({ year, title, company, duration, details }) => {
  return (
    <div className='resumeItem'>
      <p className='resumeItemP'>
        <div className='resumeItemCol'>
          <h3 className='resumeItemTitle uppercase roboto'>{company}</h3>
          <span className='resumeItemSubTitle'>{title}</span>
        </div>

        <span className='resumeItemYear'>{year}</span>
        {/* <span className='workItemDuration'>{duration}</span> */}
      </p>

      <p className='resumeItemDetails'>
        {details}
      </p>
    </div>
  )
};

export default WorkItem;

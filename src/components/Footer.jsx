
const Footer = () => {
  return (
    <footer className='footer'>
      <p className='footerP nowrap'>Copyright &copy; Cape Monn 2023</p>
    </footer>
  )
}

export default Footer;

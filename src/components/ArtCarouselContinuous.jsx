import ArtItem from "./ArtItem"
import { useState, useEffect } from "react"
import artData from '../data/artData.jsx'
import '../css/carouselContinuous.css'

const ArtCarousel = () => {

  // const numArtImages = 9; // Number of images you want to generate
  // const artImages = [];

  // for (let i = 1; i <= numArtImages; i++) {
  //   const className = `image-wrapper img${i} animation`;
  //   const altText = `Image ${i}`;

  //   artImages.push(
  //     <li key={i} className={className}>
  //       <img src={`http://placehold.it/100?text=${i}`} alt={altText} />
  //     </li>
  //   );
  // }

  return (
    <div id="slider">
      <ul className='overflowHidden'>
        <li className="image-wrapper img1 animation">
          <img src={artData[0].img} alt="" />
        </li>
        <li className="image-wrapper img2 animation">
          <img src={artData[1].img} alt="" />
        </li>
        <li className="image-wrapper img3 animation">
          <img src={artData[2].img} alt="" />
        </li>
        <li className="image-wrapper img4 animation">
          <img src={artData[3].img} alt="" />
        </li>
        <li className="image-wrapper img5 animation">
          <img src={artData[7].img} alt="" />
        </li>
        <li className="image-wrapper img6 animation">
          <img src="http://placehold.it/500?text=6" alt="" />
        </li>
        <li className="image-wrapper img7 animation">
          <img src="http://placehold.it/300?text=7" alt="" />
        </li>
        <li className="image-wrapper img8 animation">
          <img src="http://placehold.it/300?text=8" alt="" />
        </li>
        <li className="image-wrapper img9 animation">
          <img src="http://placehold.it/300?text=9" alt="" />
        </li>

        {/* <li class='image-dummy'>
          <img src="http://placehold.it/100?text=9" alt="" />
          <img src="http://placehold.it/100?text=8" alt="" />
          <img src="http://placehold.it/100?text=7" alt="" />
          <img src="http://placehold.it/100?text=6" alt="" />
          <img src="http://placehold.it/100?text=5" alt="" />
          <img src="http://placehold.it/100?text=4" alt="" />
          <img src="http://placehold.it/100?text=3" alt="" />
          <img src="http://placehold.it/100?text=2" alt="" />
          <img src="http://placehold.it/100?text=1" alt="" />
        </li> */}
      </ul>
    </div>
  )
}


export default ArtCarousel

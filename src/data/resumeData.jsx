const arrow = '⇝';
// ↘ ↝ → ⇢ ⤵ ⇒

const experienceData = [
  {
    year: `2023 ${arrow} now`,
    company: 'Freelancer',
    title: 'Web Developer / Illustrator',
    duration: 'present',
    details:
      'Web Development & Illustrator, specializing in responsive and accessible front-end design.',
    details2: 'I am currently working as a freelance illustrator and software developer. While my academic background is in Mechanical Engineering, my passion is my art. My artistic aptitude and proven knack for problem-solving uniquely qualifies me for a career in full-stack engineering.',
  },
  {
    year: `2021 ${arrow} 2023`,
    company: 'GE Aerospace',
    title: 'Manufacturing Engineer',
    duration: '2 Years',
    details:
      'Owned legacy cooling plate programs for the T700 military helicopter engine. Responsible for program creation, debug and upgrades.',
    details2: 'At GE Aerospace, I served as Manufacturing Engineer from Jan 2021 to Dec 2022. I successfully resolved complex challenges by debugging and implementing numerous legacy cooling plate programs for the T700 jet engine. My experience in advanced manufacturing and problem-solving in the aerospace industry positions me well for future endeavors.',
  },
  // {
  //   year: '2017',
  //   title: 'Protoype Engineer',
  //   duration: '3 Years',
  //   details:
  //     ''

  // },
  {
    year: 'summer 2019',
    company: 'GE Aerospace',
    title: 'Manufacturing Eng Intern',
    duration: 'Summer 2019',
    details:
      ''
  },
  {
    year: `2017 ${arrow} 2020`,
    company: 'Boston University',
    title: 'Protoype Engineer',
    duration: '3 Years',
    details2:
      'At Boston University, I served as a Prototype Engineer from 2017 to 2020. In this role, I led part manufacturing for 20+ university groups, providing guidance and instruction to researchers and students. I played a crucial role in verifying design functionality, while also leading the creation of 3D modeling and CNC programs.',
    details: 'Oversaw part manufacturing for 20+ university and research groups. Verified design functionality while leading 3D modeling and CNC programming.',
  }
];

const educationData = [
  {
    title: 'Fullstack Software Program',
    school: 'Galvanize',
    time: `Feb ${arrow} May 2023`,
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vitae esse incidunt nulla sed explicabo expedita obcaecati sunt, est laboriosam, nemo dolorum magni soluta mollitia quos omnis temporibus officiis, veritatis consectetur.',
  },
  {
    title: 'B.S. Mechanical Engineering',
    school: 'Boston University',
    time: `2016 ${arrow} 2020`,
    description: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vitae esse incidunt nulla sed explicabo expedita obcaecati sunt, est laboriosam, nemo dolorum magni soluta mollitia quos omnis temporibus officiis, veritatis consectetur.',
  },
]

const skillsData = [
  {
    category: 'LANGUAGES',
    name: [
      'Python',
      'Javascript',
      'SQL',
      'HTML5/CSS',
      'Matlab',
    ]
  },
  {
    category: 'front-end',
    name: [
      'ReactJS',
      'REDUX',
      'Bootstrap',
      'Tailwind',
      'GraphQL',

    ],

  },
  {
    category: 'back-end',
    name: [
      'Django4',
      'PostgreSQL',
      'MongoDB',
      'FastAPI',
    ],

  },
  {
    category: 'software / tools',
    name: [
      'GIT',
      'Figma',
      'WordPress',
    ],

  },
]

export { experienceData, educationData, skillsData };

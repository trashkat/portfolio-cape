import pebble_linenink from '../assets/pebble_linenink.jpeg'
import pebble_linenink_tiny from '../assets/pebble_linenink_tiny.jpg'
import moonlight_harvest from '../assets/illustrations/moonlight_harvest.png'
import moonlight_harvest_tiny from '../assets/illustrations/moonlight_harvest_tiny.jpeg'
import mask_future_i_tiny from '../assets/illustrations/mask_future_i_tiny.jpg'
import mask_future_i from '../assets/illustrations/mask_future_i.jpg'
import mask_future_ii_tiny from '../assets/illustrations/mask_future_ii_tiny.jpg'
import mask_future_ii from '../assets/illustrations/mask_future_ii.jpg'
import mask_future_iii_tiny from '../assets/illustrations/mask_future_iii_tiny.jpg'
import mask_future_iii from '../assets/illustrations/mask_future_iii.jpg'
import paladin_tiny from '../assets/illustrations/paladin_tiny.jpg'
import paladin from '../assets/illustrations/paladin.jpg'
import reflection_eternal from '../assets/illustrations/reflection_eternal.jpg'
import reflection_eternal_tiny from '../assets/illustrations/reflection_eternal_tiny.jpg'
import furys_firedrakes_tiny from '../assets/illustrations/furys_firedrakes_tiny.jpg'
import furys_firedrakes from '../assets/illustrations/furys_firedrakes.jpg'
import moon_mothers from '../assets/illustrations/moon_mothers.jpg'
import moon_mothers_tiny from '../assets/illustrations/moon_mothers_tiny.jpg'
import the_offering from '../assets/illustrations/the_offering.jpg'
import the_offering_tiny from '../assets/illustrations/the_offering_tiny.jpg'
import times_sand_tiny from '../assets/illustrations/times_sand_tiny.jpg'
import times_sand from '../assets/illustrations/times_sand.jpg'


const artData = [
  {
    loadingImg: moonlight_harvest_tiny,
    img: moonlight_harvest,
    title: 'Moonlight Harvest',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Est commodi qui magni ipsam iure quia fugiat labore similique, dolorem quibusdam magnam aperiam minima doloribus laboriosam accusamus, recusandae consequuntur aspernatur adipisci!',
    year: '2020',
  },
  {
    loadingImg: mask_future_i_tiny,
    img: mask_future_i,
    title: 'Masks of our Future Selves I',
    description: '',
    year: '2022',
  },
  {
    loadingImg: mask_future_iii_tiny,
    img: mask_future_iii,
    title: 'Masks of our Future Selves III',
    description: '',
    year: '2022',
  },
  {
    loadingImg: furys_firedrakes_tiny,
    img: furys_firedrakes,
    title: `Fury's Firedrakes`,
    description: '',
    year: '2020',
  },
  {
    loadingImg: paladin_tiny,
    img: paladin,
    title: 'Paladin',
    description: '',
    year: '2022',
  },
  {
    loadingImg: reflection_eternal_tiny,
    img: reflection_eternal,
    title: 'Reflection Eternal',
    description: '',
    year: '2021',
  },
  {
    loadingImg: mask_future_ii_tiny,
    img: mask_future_ii,
    title: 'Masks of our Future Selves II',
    description: '',
    year: '2022',
  },
  {
    loadingImg: moon_mothers_tiny,
    img: moon_mothers,
    title: 'Moon Mothers',
    description: '',
    year: '2023',
  },
  {
    loadingImg: the_offering_tiny,
    img: the_offering,
    title: 'The Offering',
    description: '',
    year: '2020',
  },
  {
    loadingImg: times_sand_tiny,
    img: times_sand,
    title: `Time's Sand`,
    description: '',
    year: '2022',
  },
  {
    loadingImg: pebble_linenink_tiny,
    img: pebble_linenink,
    title: 'more coming soon!',
    description: '',
    year: '',
  },
];


export default artData;
